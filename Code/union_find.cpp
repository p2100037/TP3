#include "union_find.hpp"


union_find::union_find(int x, int y){
    rang.resize(x*y,0);
   for(int i = 0; i < x*y; i++){
        tab.push_back(i);
    }

}

void union_find::afficher(){
    cout << "[ ";  
    for(int i = 0; i < tab.size(); i++){
        
        cout << tab[i] << " ";
        
    }
    //cout << tab[tab.size()-1] << " ";
    cout << "]" << endl; 
}

void union_find::inserer(int n){
    tab.push_back(n);

}
//////////////////////////////////////////////////////////////////////////////////////////////
int union_find::racine(int n){
    vector<int> temp;
    int elem_courant = n;
   do{
        //cout << "Le pere de " << elem_courant << " est: " << tab[elem_courant] << endl;
        temp.push_back(elem_courant);
        elem_courant = tab[elem_courant];
        
        
    }while(elem_courant != tab[elem_courant]);

    for(int i = 0; i < temp.size(); i++){
        tab[temp[i]] = tab[elem_courant];
    }

    //cout << "la racine de " << n << " est: " << tab[elem_courant] << endl;
    return tab[elem_courant];


}

void union_find::reunir(int x, int y){
    int racinex = racine(x);
    int raciney = racine(y);
    if (racinex != raciney) {
        // Union by rang
        if (rang[racinex] < rang[raciney])
            tab[racinex] = raciney;
        else if (rang[racinex] > rang[raciney])
            tab[raciney] = racinex;
        else {
            tab[raciney] = racinex;
            rang[racinex]++;
        }
    }
}

bool union_find::est_dans_meme_ens(int x, int y){
    return racine(x) == racine(y);
}

void union_find::vider(){
    tab.clear();
}