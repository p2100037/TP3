#ifndef UNION_FIND_HPP_
#define UNION_FIND_HPP_

#include <vector>
#include <iostream>

using namespace std;

class union_find {
    private:
    vector<int> tab;
    vector<int> rang;

    public:
    union_find(int x, int y);
    void inserer(int n);
    int racine(int n);
    void reunir(int x, int y);
    bool est_dans_meme_ens(int x, int y);
    void afficher();
    void vider();

};

#endif